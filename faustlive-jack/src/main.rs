#![feature(box_into_inner)]
#![feature(slice_ptr_get)]

use rtrb::Producer;
use inotify::{Inotify, WatchMask};
use rtrb::Consumer;
use libfaustllvm::*;
use std::time::Duration;
use std::thread;
use jack::*;

pub struct Logger {
    name: String
}

impl Logger {
    pub fn new(name: String) -> Logger {
        Logger { name }
    }
    pub fn _log(&self, level: &str, message: &str) {
        println!("[{}::{}] {}", self.name, level, message);
    }
    pub fn info(&self, message: &str) {
        self._log("info", message);
    }
    pub fn debug(&self, message: &str) {
        self._log("debug", message);
    }
    pub fn warn(&self, message: &str) {
        self._log("warn", message);
    }
    pub fn error(&self, message: &str) {
        self._log("error", message);
    }
}

impl Clone for Logger {
    fn clone(&self) -> Logger {
        Logger {
            name: self.name.clone()
        }
    }
}

pub fn logger(name: String) -> Box<dyn Fn(&str)>  {
    let log = move |message: &str| println!("[{}] {}", name, message);
    Box::new(log)
}


pub struct DSP {
    faust: FaustLLVMInstance, 
    ports_in: Vec<Vec<f32>>,
    ports_out: Vec<Vec<f32>>,
    buffer_input: Vec<*const f32>,
    buffer_output: Vec<*mut f32>,
    max_buffer_size: usize,
    sample_rate: usize
}

unsafe impl Send for DSP {}

impl DSP {
    fn from_file(file: &str, sample_rate: usize, max_buffer_size: usize) -> Result<DSP, Box<dyn std::error::Error>> {
        let factory: FaustLLVMFactory = FaustLLVMFactory::from_file(file)?;
        Ok(DSP::from_factory(factory, sample_rate, max_buffer_size))
    }
    fn from_factory(factory: FaustLLVMFactory, sample_rate: usize, max_buffer_size: usize) -> DSP {
        let mut faust: FaustLLVMInstance = factory.into(); 
        faust.init(sample_rate);

        let num_inputs = faust.inputs();
        let num_outputs = faust.outputs();

        let mut ports_in: Vec<Vec<f32>> = vec![vec![0_f32;  max_buffer_size]; num_inputs];
        let mut ports_out: Vec<Vec<f32>> = vec![vec![0_f32; max_buffer_size]; num_outputs];
        let buffer_input: Vec<*const f32> = vec![0_f32; num_inputs].into_iter().enumerate().map(|(i, _)| {
            ports_in[i].as_ptr()
        }).collect();
        let buffer_output = vec![0_f32; num_outputs].into_iter().enumerate().map(|(i, _)| {
            ports_out[i].as_mut_ptr()
        }).collect();
        println!("len buffer_input: {}", buffer_input.len());

        DSP {
            faust,
            ports_in,
            ports_out,
            buffer_input,
            buffer_output,
            max_buffer_size, 
            sample_rate
        }


    }

}

pub struct System {
    dsp: DSP,
    dsp_consumer: Consumer<DSP>,
    ports_out: Vec<Port<AudioOut>>,
    ports_in: Vec<Port<AudioIn>>,
    count: usize
}

unsafe impl Send for System {}

impl System {
    pub fn new() {
    }
}

impl NotificationHandler for System {
    fn thread_init(&self, _client: &Client) {
    }
}

impl ProcessHandler for System {
    fn process(&mut self, client: &jack::Client, scope: &ProcessScope) -> Control {
        if let Ok(dsp) = self.dsp_consumer.pop() {
            self.dsp = dsp;
        }
        // Plump together jack inputs/outputs with faust inputs/outputs
        for i in 0..std::cmp::min(self.dsp.faust.inputs(), self.ports_in.len()) {
            self.dsp.buffer_input[i] = self.ports_in[i].as_slice(scope).as_ptr() as *const f32; 
        }
        for i in 0..std::cmp::min(self.dsp.faust.outputs(), self.ports_out.len()) {
            self.dsp.buffer_output[i] = self.ports_out[i].as_mut_slice(scope).as_mut_ptr() as *mut f32; 
        }
        self.dsp.faust.compute(self.count, self.dsp.buffer_input.as_slice().as_ptr() as *const *const f32, self.dsp.buffer_output.as_mut_slice().as_mut_ptr() as *mut *mut f32);
        Control::Continue
    }
}

pub fn setup_jack(mut dsp_consumer: Consumer<DSP>) -> Result<jack::AsyncClient<(), System>, jack::Error>{
    let logger = Logger::new("setup_jack".to_string());
    let (client, _st) = jack::Client::new("faustlive", jack::ClientOptions::NO_START_SERVER)?;

    logger.info(&format!("jack sample rate: {}", client.sample_rate()));
    logger.info("Waiting for dsp");
    let dsp: DSP = loop {
        if let Ok(dsp) = dsp_consumer.pop() {
            break dsp
        }
        thread::sleep(Duration::from_millis(100));
    };
    let mut ports_in = Vec::new();
    for i in 0..dsp.faust.inputs() {
        ports_in.push(client.register_port(&format!("in_{}", i), AudioIn)?);
    } 

    let mut ports_out = Vec::new();
    for i in 0..dsp.faust.outputs() {
        ports_out.push(client.register_port(&format!("out_{}", i), AudioOut)?);
    } 

    let count = client.buffer_size() as usize;

    let system = System { 
        dsp,
        dsp_consumer,
        count,
        ports_in,
        ports_out,
    };
    let client = client.activate_async((), system)?;
    Ok(client)
}

fn main() {
    // Create a client and a handler
    let (mut dsp_producer, mut dsp_consumer) = rtrb::RingBuffer::new(2);

    let thread_hotreload = thread::spawn(move || {
        let logger = Logger::new("thread_hotreload".to_string());
        let file = "./test.dsp";

        fn reload_and_send(logger: Logger, file: &str, dsp_producer: &mut Producer<DSP>) {
            let mut dsp = match DSP::from_file(file, 48000, 1024) {
                Ok(factory) => factory,
                Err(e) => {
                    logger.error(&e.to_string());
                    return;
                }
            };

            if let Err(e) = dsp_producer.push(dsp) {
                println!("[thread_hotreload] Error: {:?}", e);
                return;
            };
        }
        reload_and_send(logger.clone(), file, &mut dsp_producer);
        logger.info("Start watching");
        // Read events that were added with `add_watch` above.
        let mut inotify = Inotify::init()
            .expect("Error while initializing inotify instance");
        inotify
            .add_watch(
                file,
                WatchMask::MODIFY,
            )
            .expect("Failed to add file watch");
        let mut buffer = [0; 1024];
        loop {
            let events = inotify.read_events_blocking(&mut buffer)
                .expect("Error while reading events");


            for event in events {
                logger.debug(&format!("event: {:?}", event));
                reload_and_send(logger.clone(), file, &mut dsp_producer);
            }
        }
        println!("[thread_hotreload] ciao.");
    });

    let client = setup_jack(dsp_consumer);
    let logger = Logger::new("main".to_string());
    logger.info("setup jack");
    
    // An active async client is created, `client` is consumed.
    thread_hotreload.join().unwrap();
}
